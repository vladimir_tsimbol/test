﻿using System;


namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            var calculator = new Engine.Calculator();

            while(true)
            {
                Console.WriteLine(@"Введите выражение или ""q"" для выхода");
                var expression = Console.ReadLine();

                if (expression == "q")
                    return;
                else
                {
                    //Вычисление, вывод результата
                    var report = calculator.Calculate(expression);
                    report.Processing();
                }
            }
        }
    }
}
