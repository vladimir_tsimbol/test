﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Calculator.Engine;
using Calculator.Engine.Checking;
using Calculator.Engine.Parser;
using Calculator.Engine.Operations;


namespace EngineTest
{
    [TestClass]
    public class CalculatorEngineTest
    {
        OperatorCollection operators = new OperatorCollection()
                        .Add(Operator.Create<AddOperation>(@"+", Priority.Lowest))
                        .Add(Operator.Create<SubtractOperation>(@"-", Priority.Lowest))
                        .Add(Operator.Create<MultiplicateOperation>(@"*", Priority.High))
                        .Add(Operator.Create<DivideOperation>("/", Priority.High));


        [TestMethod]
        public void TestAddOperation()
        {
            double x        = 1.5;
            double y        = 2.5;
            double expected = 4.0;

            AddOperation addOperation = new AddOperation(x, y);
            double       actual       = addOperation.GetValue();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestSubtractOperation()
        {
            double x        = 1.5;
            double y        = 2.5;
            double expected = -1.0;

            SubtractOperation subtractOperation = new SubtractOperation(x, y);
            double            actual            = subtractOperation.GetValue();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestMultiplicateOperation()
        {
            double x        = 1.5;
            double y        = 2.0;
            double expected = 3.0;

            MultiplicateOperation multiplicateOperation = new MultiplicateOperation(x, y);
            double                actual                = multiplicateOperation.GetValue();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestDivideOperation()
        {
            double x        = 3.0;
            double y        = 1.5;
            double expected = 2.0;

            DivideOperation divideOperation = new DivideOperation(x, y);
            double          actual          = divideOperation.GetValue();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException))]
        public void TestDivideByZeroOperation()
        {
            double x = 8.0;
            double y = 0;

            DivideOperation divideOperation = new DivideOperation(x, y);
            divideOperation.GetValue();
        }


        [TestMethod]
        public void TestCheckingExpressionWithBrackets()
        {
            string expression = @"((1-2)*(-3)+4-(5/(10+(-5))))/(4-2)";
            string expected   = @"((1-2)*~3+4-(5/(10+~5)))/(4-2)";

            Checking checking = new Checking(operators);
            string   actual   = expression;
            checking.Check(ref actual);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestCheckingExpressionWithDoubleMinus()
        {
            string expression = @"--1--1";

            Checking checking = new Checking(operators);
            checking.Check(ref expression);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestCheckingIncorrectBrackets()
        {
            string expression = @"(1+1";
            
            Checking checking = new Checking(operators);
            checking.Check(ref expression);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestCheckingEmptyBrackets()
        {
            string expression = @"1 + ()";

            Checking checking = new Checking(operators);
            checking.Check(ref expression);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestCheckingIncorrectOperandType1()
        {
            string expression = @".0+8";

            Checking checking = new Checking(operators);
            checking.Check(ref expression);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestCheckingIncorrectOperandType2()
        {
            string expression = @"0.+8";

            Checking checking = new Checking(operators);
            checking.Check(ref expression);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestCheckingIncorrectOperandType3()
        {
            string expression = @"0..8";

            Checking checking = new Checking(operators);
            checking.Check(ref expression);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestCheckingMissingOperatorType1()
        {
            string expression = @"0(2+8)";

            Checking checking = new Checking(operators);
            checking.Check(ref expression);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestCheckingMissingOperatorType2()
        {
            string expression = @"(2+8)0";

            Checking checking = new Checking(operators);
            checking.Check(ref expression);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestCheckingMissingOperandType1()
        {
            string expression = @"*(1+2)";

            Checking checking = new Checking(operators);
            checking.Check(ref expression);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestCheckingMissingOperandType2()
        {
            string expression = @"2 + 7 -";

            Checking checking = new Checking(operators);
            checking.Check(ref expression);
        }


        [TestMethod]
        public void TestParserPriorityOperations()
        {
            string expression = @"1 + 2 * 3 - 4 / 5";
            double expected   = 6.2;

            Parser parser = new Parser(operators);
            double actual = parser.Parse(expression);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestParserExpressionWithBrackets()
        {
            string expression = @"((1-2)*~3+4-(5/(10+~5)))/(4-2)";
            double expected   = 3.0;

            Parser parser = new Parser(operators);
            double actual = parser.Parse(expression);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestParserNestedBrackets()
        {
            string expression = @"((((1+5))))";
            double expected   = 6;

            Parser parser = new Parser(operators);
            double actual = parser.Parse(expression);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestParserSingleValue()
        {
            string expression = @"1.2345";
            double expected   = 1.2345;

            Parser parser = new Parser(operators);
            double actual = parser.Parse(expression);

            Assert.AreEqual(expected, actual);
        }
    }
}
