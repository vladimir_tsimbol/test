﻿using System;


namespace Calculator.Engine.Operations
{
    public class AddOperation : BinaryOperation
    {
        public AddOperation(double leftOperand, double rightOperand)
            : base(leftOperand, rightOperand)
        {
        }


        public override double GetValue()
        {
            return LeftOperand + RightOperand;
        }
    }


    public class SubtractOperation : BinaryOperation
    {
        public SubtractOperation(double leftOperand, double rightOperand) 
            : base(leftOperand, rightOperand)
        {
        }


        public override double GetValue()
        {
            return LeftOperand - RightOperand;
        }
    }


    public class MultiplicateOperation : BinaryOperation
    {
        public MultiplicateOperation(double leftOperand, double rightOperand) 
            : base(leftOperand, rightOperand)
        {
        }


        public override double GetValue()
        {
            return LeftOperand * RightOperand;
        }
    }


    public class DivideOperation : BinaryOperation
    {
        public DivideOperation(double leftOperand, double rightOperand) 
            : base(leftOperand, rightOperand)
        {
        }


        public override double GetValue()
        {
            if (RightOperand == 0.0)
                throw new DivideByZeroException(@"Деление на ноль");
            return LeftOperand / RightOperand;
        }
    }
}