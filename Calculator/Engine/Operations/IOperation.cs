﻿namespace Calculator.Engine.Operations
{
    public interface IOperation
    {
        double GetValue();
    }
}