﻿namespace Calculator.Engine.Operations
{
    public abstract class BinaryOperation : IOperation
    {
        protected double LeftOperand { get; set; }
        protected double RightOperand { get; set; }


        protected BinaryOperation(double leftOperand, double rightOperand)
        {
            this.LeftOperand  = leftOperand;
            this.RightOperand = rightOperand;
        }


        public abstract double GetValue();
    }
}