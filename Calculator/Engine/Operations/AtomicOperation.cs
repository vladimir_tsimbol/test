﻿namespace Calculator.Engine.Operations
{
    public abstract class AtomicOperation : IOperation
    {
        protected double Operand { get; set; }


        protected AtomicOperation(double operand)
        {
            this.Operand = operand;
        }


        public abstract double GetValue();
    }
}