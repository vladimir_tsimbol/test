﻿using System;
using Calculator.Engine.Operations;


namespace Calculator.Engine
{
    public sealed class Operator
    {
        private Type operationType;


        public string OperatorString { get; private set; }
        public Priority Priority { get; private set; }


        private Operator(Type operationType, string operatorString, Priority priority)
        {
            this.operationType  = operationType;
            OperatorString = operatorString;
            Priority       = priority;
        }


        public static Operator Create<T>(string operatorString, Priority priority) where T : IOperation
        {
            return new Operator(typeof(T), operatorString, priority);
        }
        public IOperation Compute(double left, double right)
        {
            var constructorInfo = operationType.GetConstructor(new Type[] { left.GetType(), right.GetType() });
            var operation = constructorInfo.Invoke(new object[] { left, right });

            return (IOperation)operation;
        }
    }
}