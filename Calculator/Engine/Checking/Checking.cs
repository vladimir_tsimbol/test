﻿using System;
using System.Text.RegularExpressions;


namespace Calculator.Engine.Checking
{
    public sealed class Checking : IChecking
    {
        public OperatorCollection OperatorCollection { get; set; }


        public Checking(OperatorCollection operators)
        {
            OperatorCollection = operators;
        }


        public void Check(ref string expression)
        {
            //Проверка длины
            if (String.IsNullOrWhiteSpace(expression))
                throw new ArgumentNullException(@"Пустое выражение");

            //Убираем пробелы
            expression = expression.Replace(" ", "");

            //Проверяем корректность символов (ищем неподдерживаемые)
            string pattern = @"\d\,\.\(\)";
            foreach (var item in OperatorCollection)
                pattern = String.Concat(pattern, @"\" + item.OperatorString);
            if (Regex.IsMatch(expression, @"[^" + pattern + @"]+"))
                throw new ArgumentException(@"Нарушение синтаксиса");

            //Подсчет скобок, проверяем, чтобы были все открывающие и закрывающие
            int bracketsCount = 0;
            for (int i = 0; i < expression.Length; i++)
            {
                if (expression[i] == '(')
                    bracketsCount++;
                else if (expression[i] == ')')
                    bracketsCount--;
            }
            if (bracketsCount != 0)
                throw new ArgumentException(@"Нарушение синтаксиса");
            //Проверяем наличие скобок с пустыми выражениями
            if (Regex.IsMatch(expression, @"\({1}\){1}"))
                throw new ArgumentException(@"Нарушение синтаксиса");

            //Проверяем корректность введенного выражения
            //Двойное отрицание
            if (expression.Contains(@"--"))
                throw new ArgumentException(@"Нарушение синтаксиса");
            //Пропуски операторов, операндов, некорректная запись вещественных чисел
            for (int i = 0; i < expression.Length; i++)
            {
                if (expression[i] == '.' || expression[i] == ',')
                {
                    if (i == 0 || i == expression.Length - 1)
                        throw new ArgumentException(@"Нарушение синтаксиса");               //,0...  ...0,
                    else if (i > 0 && i < expression.Length - 1)
                    {
                        if ((expression[i - 1] < '0' || expression[i - 1] > '9') || (expression[i + 1] < '0' || expression[i + 1] > '9'))       //...0,...  ...,0...   ...,...
                            throw new ArgumentException(@"Нарушение синтаксиса");
                    }
                }
                else if (expression[i] == ')')
                {
                    if (i == 0)
                        throw new ArgumentException(@"Нарушение синтаксиса");               //)...
                    if (i < expression.Length - 1)
                    {
                        if (expression[i + 1] >= '0' && expression[i + 1] <= '9')           //...)0...
                            throw new ArgumentException(@"Нарушение синтаксиса");
                    }
                    if (i > 0)
                    {
                        if (!((expression[i - 1] >= '0' && expression[i - 1] <= '9') || expression[i - 1] == ')'))      //...+)...
                            throw new ArgumentException(@"Нарушение синтаксиса");
                    }
                }
                else if (expression[i] == '(')
                {
                    if (i > 0)
                    {
                        if (expression[i - 1] >= '0' && expression[i - 1] <= '9')            //...0(...
                            throw new ArgumentException(@"Нарушение синтаксиса");
                    }
                    if (i < expression.Length - 1)
                    {
                        if (!(expression[i + 1] == '-' || expression[i + 1] == '(' || (expression[i + 1] >= '0' && expression[i + 1] <= '9')))      //...(+
                            throw new ArgumentException(@"Нарушение синтаксиса");
                    }
                }

                if (!(expression[i] == '-' || expression[i] == '(' || (expression[i] >= '0' && expression[i] <= '9')))
                {
                    if (i == 0)
                        throw new ArgumentException(@"Нарушение синтаксиса");               //+0
                }

                if (!(expression[i] == ')' || (expression[i] >= '0' && expression[i] <= '9')))
                {
                    if (i == expression.Length - 1)
                        throw new ArgumentException(@"Нарушение синтаксиса");               //...+
                }
            }

            //Избавляемся от скобок, обрамляющих отрицательные значения + все отрицательные записываем через ~
            //Проверка на отрицание в начале строки
            if (expression[0] == '-')
                expression = @"~" + expression.Substring(1, expression.Length - 1);
            //Ищем места: (-1.2345) -> ~1.2345
            while (true)
            {
                pattern = @"[\(\-]{2}[\d]+[\.\,]?[\d]*[\)]?";
                if (Regex.IsMatch(expression, pattern))
                {
                    int index = expression.IndexOf(@"(-");
                    if (index != -1)
                    {
                        for (int i = index + 2; i < expression.Length; i++)
                        {
                            if (!((expression[i] >= '0' && expression[i] <= '9') || expression[i] == '.' || expression[i] == ','))
                            {
                                if (expression[i] == ')')
                                    expression = expression.Substring(0, index) + @"~" + expression.Substring(index + 2, i - index - 2) + expression.Substring(i + 1);
                                else expression = expression.Substring(0, index + 1) + @"~" + expression.Substring(index + 2);
                                break;
                            }
                        }
                    }
                    else break;
                }
                else break;
            }
        }
    }
}