﻿namespace Calculator.Engine.Checking
{
    public interface IChecking
    {
        OperatorCollection OperatorCollection { get; set; }

        void Check(ref string expression);
    }
}