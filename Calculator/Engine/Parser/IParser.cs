﻿namespace Calculator.Engine.Parser
{
    public interface IParser
    {
        OperatorCollection OperatorCollection { get; set; }

        double Parse(string expression);
    }
}