﻿using System;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;


namespace Calculator.Engine.Parser
{
    public sealed class Parser : IParser
    {
        private readonly string[] operatorString;


        public OperatorCollection OperatorCollection { get; set; }


        public Parser(OperatorCollection operators)
        {
            OperatorCollection = operators;

            int index = 0;
            operatorString = new string[OperatorCollection.Count];
            foreach (var item in OperatorCollection)
            {
                operatorString[index] = item.OperatorString;
                index++;
            }
        }


        public double Parse(string expression)
        {
            return BinaryParse(ref expression);
        }

        //Парсинг, рекурсивное вычисление всех бинарных операций (выполнять в конце, на случай если будут добавлены тригонометрические, корни, факториалы)
        private double BinaryParse(ref string expression)
        {
            if (expression.Contains(")"))
            {
                //----В ПЕРВУЮ ОЧЕРЕДЬ РАСКРЫВАЕМ ВСЕ ВЫРАЖЕНИЯ В СКОБКАХ-------------------
                //Достаем выражение в скобках (функция возвращает подстроку без скобок)
                string expressionPart = GetExpressionInBracket(expression);

                //Достаем значения и операторы в подстроке
                List<double> valueList;
                List<string> operatorList;
                FindBinaryOperandsAndOperators(expressionPart, out valueList, out operatorList);

                //От количества определяем дальнейшие действия
                if (valueList.Count == 1)
                {
                    //Обработка: (1.234) -> 1.234
                    expression = expression.Replace(@"(" + expressionPart + @")", DoubleToString(valueList[0]));
                    return BinaryParse(ref expression);
                }
                else if (valueList.Count == 2)
                {
                    //Обработка: (1.2+1.2) -> 2.4
                    double value = OperatorCollection.Get(operatorList[0]).Compute(valueList[0], valueList[1]).GetValue();
                    expression   = expression.Replace(@"(" + expressionPart + @")", DoubleToString(value));
                    return BinaryParse(ref expression);
                }
                else
                {
                    //Обработка: выражения с количеством действий более 2х
                    string newExpressionPart = ProcessingLongExpression(ref valueList, ref operatorList);
                    expression               = expression.Replace(expressionPart, newExpressionPart);
                    return BinaryParse(ref expression);
                }
            }
            else
            {
                //----ВЫРАЖЕНИЯ В СКОБКАХ ОТСУТСТВУЮТ---------------------------------------
                //Достаем значения и операторы
                List<double> valueList;
                List<string> operatorList;
                FindBinaryOperandsAndOperators(expression, out valueList, out operatorList);

                if (valueList.Count == 1)
                    return valueList[0];
                else if (valueList.Count == 2)
                    return OperatorCollection.Get(operatorList[0]).Compute(valueList[0], valueList[1]).GetValue();
                else
                {
                    expression = ProcessingLongExpression(ref valueList, ref operatorList);
                    return BinaryParse(ref expression);
                }
            }
        }
        
        //Функция получения выражения в скобках
        private string GetExpressionInBracket(string expression)
        {
            for (int i = 0; i < expression.Length; i++)
            {
                if (expression[i] == ')')
                {
                    for (int j = i; j >= 0; j--)
                    {
                        if (expression[j] == '(')
                            return expression.Substring(j + 1, i - j - 1);
                    }
                }
            }

            return expression;
        }
        //Функция парсинга числовых значений и операторов - на вход подается выражение без скобок
        private void FindBinaryOperandsAndOperators(string expression, out List<double> operands, out List<string> operators)
        {
            operands  = new List<double>();
            operators = new List<string>();

            //Парсинг значений
            string[] arrayValues = expression.Split(operatorString, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < arrayValues.Length; i++)
            {
                double tempValue = 0;
                if (DoubleParse(arrayValues[i], out tempValue))
                    operands.Add(tempValue);
                else throw new ArgumentException(@"Нарушение синтаксиса");
            }

            //Парсинг операторов
            expression = expression.Remove(0, arrayValues[0].Length);
            for (int i = 1; i < arrayValues.Length; i++)
            {
                int    stopIndex    = expression.IndexOf(arrayValues[i]);
                string tempOperator = operatorString.FirstOrDefault(x => x.Equals(expression.Substring(0, stopIndex)));
                if (tempOperator == null)
                    throw new ArgumentException(@"Неизвестный оператор");
                else operators.Add(tempOperator);
                expression = expression.Remove(0, stopIndex + arrayValues[i].Length);
            }

            //Проверка взаимного количества значений и операторов
            if (operands.Count - 1 != operators.Count)
                throw new ArgumentException(@"Некорректное количество операций и операторов");
        }
        //Валидация и парсинг числовых значений
        private bool DoubleParse(string data, out double output)
        {
            output = 0.0;

            //Убираем двойные отрицания
            if (data.Contains("~~"))
                data = data.Replace("~~", "");

            //Возвращаем стандартный оператор отрицания
            data = data.Replace('~', '-');

            data = data.Replace(".", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
            data = data.Replace(",", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);

            try   { output = double.Parse(data, CultureInfo.CurrentCulture); }
            catch { return false; }

            return true;
        }
        //Перевод значения из double в string
        private string DoubleToString(double value)
        {
            if (value < 0)
                return @"~" + Math.Abs(value).ToString(CultureInfo.CurrentCulture);
            else return Math.Abs(value).ToString(CultureInfo.CurrentCulture);
        }
        //Функция вычисления значений выражений, включающих два и более операторов - на вход подается выражение без скобок
        private string ProcessingLongExpression(ref List<double> valueList, ref List<string> operatorList)
        {
            //Находим первую самую приоритетную операцию
            Operator[] operators = new Operator[operatorList.Count];
            for (int i = 0; i < operatorList.Count; i++)
                operators[i] = OperatorCollection.Get(operatorList[i]);
            for (Priority p = Priority.High; p <= Priority.Lowest; p++)
            {
                for (int i = 0; i < operators.Length; i++)
                {
                    if (operators[i].Priority == p)
                    {
                        //Выполнение операции
                        valueList[i] = operators[i].Compute(valueList[i], valueList[i + 1]).GetValue();
                        
                        //Удаляем операнды и оператор
                        valueList.RemoveAt(i + 1);
                        operatorList.RemoveAt(i);

                        //Преобразуем строку, полученную в качестве аргумента
                        string expression = String.Empty;
                        for (int j = 0; j < operatorList.Count; j++)
                            expression = String.Concat(expression, DoubleToString(valueList[j]) + operatorList[j]);
                        expression = String.Concat(expression, DoubleToString(valueList[valueList.Count - 1]));
                        return expression;
                    }
                }
            }

            throw new ArgumentException(@"Ошибка при вычислении");
        }
    }
}