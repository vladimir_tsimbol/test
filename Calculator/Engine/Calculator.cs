﻿using System;
using Calculator.Engine.Parser;
using Calculator.Engine.Checking;
using Calculator.Engine.Reporter;
using Calculator.Engine.Operations;


namespace Calculator.Engine
{
    public class Calculator
    {
        private readonly IParser            parser;
        private readonly IChecking          checking;
        private readonly OperatorCollection operators;
        private IProcessingReport           processingReport;


        public Calculator()
        {
            this.operators = new OperatorCollection()
                                    .Add(Operator.Create<AddOperation>(@"+", Priority.Lowest))
                                    .Add(Operator.Create<SubtractOperation>(@"-", Priority.Lowest))
                                    .Add(Operator.Create<MultiplicateOperation>(@"*", Priority.High))
                                    .Add(Operator.Create<DivideOperation>("/", Priority.High));

            this.checking         = new Checking.Checking(operators);
            this.parser           = new Parser.Parser(operators);
            this.processingReport = new ConsoleReport();
        }
        public Calculator(IChecking checking, IParser parser, OperatorCollection operators, IProcessingReport processingReport)
        {
            this.checking         = checking;
            this.parser           = parser;

            if (operators.Count == 0)
                throw new ArgumentNullException(@"Операции не заданы");

            this.operators        = operators;
            this.processingReport = processingReport;
        }


        public Report Calculate(string expression)
        {
            try
            {
                //Проверяем, приводим к необходимому виду
                checking.Check(ref expression);
                //Вычисляем
                var result = parser.Parse(expression);
                //Создаем отчет
                return new Report(String.Empty, result, false, processingReport);
            }
            catch (ArgumentNullException exception)
            { return new Report(exception.Message, 0, true, processingReport); }
            catch (ArgumentException exception)
            { return new Report(exception.Message, 0, true, processingReport); }
            catch (DivideByZeroException exception)
            { return new Report(exception.Message, 0, true, processingReport); }
            catch
            { return new Report(@"Неизвестная ошибка", 0, true, processingReport); }
        }
    }
}