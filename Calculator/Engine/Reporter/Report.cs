﻿namespace Calculator.Engine.Reporter
{
    public class Report
    {
        private readonly string            message;
        private readonly double            value;
        private readonly bool              exceptionFlag; 
        private readonly IProcessingReport processingReport;


        public Report(string message, double value, bool exceptionFlag, IProcessingReport processingReport)
        {
            this.message          = message;
            this.value            = value;
            this.exceptionFlag    = exceptionFlag;
            this.processingReport = processingReport;
        }
        public Report(IProcessingReport processingReport)
        {
            this.processingReport = processingReport;
        }


        public void Processing()
        {
            processingReport.Processing(message, value, exceptionFlag);
        }
    } 
}