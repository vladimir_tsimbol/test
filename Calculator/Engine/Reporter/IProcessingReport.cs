﻿namespace Calculator.Engine.Reporter
{
    public interface IProcessingReport
    {
        void Processing(string message, double value, bool exceptionFlag);
    }
}