﻿using System;
using System.Globalization;


namespace Calculator.Engine.Reporter
{
    public class ConsoleReport : IProcessingReport
    {
        public void Processing(string message, double value, bool exceptionFlag)
        {
            if (exceptionFlag)
                Console.WriteLine(@"Ошибка. " + message);
            else Console.WriteLine(@"Результат: " + value.ToString(CultureInfo.CurrentCulture));
        }
    }
}