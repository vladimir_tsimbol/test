﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;


namespace Calculator.Engine
{
    public class OperatorCollection : IEnumerable<Operator>
    {
        private ICollection<Operator> operators;


        public int Count
        {
            get { return operators.Count; }
        }


        public OperatorCollection() : this(new Collection<Operator>()) { }
        public OperatorCollection(ICollection<Operator> operators)
        {
            this.operators = operators;
        }


        public OperatorCollection Add(Operator addOperator)
        {
            if (addOperator != null)
                operators.Add(addOperator);
            return this;
        }
        public Operator Get(string operatorString)
        {
            return operators.FirstOrDefault(x => x.OperatorString.Equals(operatorString));
        }


        public IEnumerator<Operator> GetEnumerator()
        {
            return operators.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return operators.GetEnumerator();
        }
    }
}