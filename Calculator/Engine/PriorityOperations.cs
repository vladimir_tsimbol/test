﻿namespace Calculator.Engine
{
    public enum Priority
    {
        Highest,                                                                            //Атомарные - знаковые операции
        High,                                                                               //Умножение, дробное деление
        Low,                                                                                //Целочисленное деление, остаток от деления
        Lowest                                                                              //Сложение, вычитание
    }
}